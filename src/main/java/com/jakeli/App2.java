package com.jakeli;

import org.checkerframework.checker.nullness.compatqual.NullableDecl;
import org.json.JSONArray;
import org.json.JSONException;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.json.JSONObject;


import java.io.*;
import java.net.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class App2 {

    private static String[] goldfishUrls = {
            "https://www.mtggoldfish.com/movers-details/paper/mh1/winners/dod",
            "https://www.mtggoldfish.com/movers-details/paper/standard/winners/dod",
            "https://www.mtggoldfish.com/movers-details/paper/modern/winners/dod",
            "https://www.mtggoldfish.com/movers-details/paper/m20/winners/dod"};

    private static String tcgplayerUrl = "https://shop.tcgplayer.com/magic/product/show?ProductName=%s&newSearch=false&ProductType=All&IsProductNameExact=true";
    private static int inc = 0;
    private static int total = 0;
    private static final String kDataRootDir = "crawlered_data/";

    private static final long kWaitTimeShort = 10;
    private static final long kWaitTimeLong = 20;//todo markmark 这里设置成20
    private static final long kWaitTimeImplicit = 5;
    private static final int kLoginTryTimes = 10;

    private static final String[] kSetIgnoreKeywords = {"Prerelease"};
    private static final String[] kAnalyzeSetIgnores = {"Invocations", "Expeditions", "Judge", "Explorers of", "Masterpiece", "Promos", "Magic Game Night", "Magic Player Rewards", "Commander", "Mountain", "Island", "Forest", "Plain"};

    private static final boolean kIsConsiderCountDiff = true;//默认：true；分析：是否开启数量差筛选功能
    private static final boolean kIsConsiderScarce = true;//默认：true；分析：是否开启总数量阈值筛选功能。
    private static final String kCondition = "and";//默认：or；分析：or后者and
    //todo 加上数量增加的增加的跟踪 “用于卖卡”
    private static final int kCountDiffThreshold = -10;//默认：-10；分析：xx小时之内，卖出数量大于一定才显示
    private static final int kShowCountThreshold = 200;//默认：200;分析：当前数量小于xxx才显示。
    private static final int kMaxInterval = 120;//默认120；删除设定72 分析：xx小时之内的时间间隔的数量差所有系列的这个在24h之内变
    //todo markmark!!!!!! 这里有问题，无论interval设置成多少，都不能筛选出真正的时间范围内的东西
    private static final int kRemoveDiffThreshold = -3;//分析：默认999或者-2，删除设定-5，就是什么都不干掉。这是>= x + 1 remove； 有一个版本 xxx小时达标了 就保留，前提是跟踪改卡片的时间超过xx小时才会启动，也就是至少保证升了的卡片关注3天，还有少于xx的不能删，如果kmaxinterval时间间隔之内，如果数量减少大于xxx，就从tocrawllist删掉卡片。当该list卡片太多才设成-2之类的，然后运行，然后设置成999，否则新卡也会被删掉。
    private static final int kMaxCountDelThreshold = 250;//  删除设定250
    private static final int kToWatchThreshold = 100;//默认100

    public static void main(String[] args) throws Exception{
        analyze();
        crawl();
        analyze();
    }

    private static void analyze(){
        try{
            //todo 这里做个逻辑吧六天之前的删掉。
            String jsonStr = fileToString("somejsondata.json");
            System.out.println("---------------开始分析-------------");
            JSONObject root = new JSONObject(jsonStr);

            Iterator<String> keyIter = root.keys();
            List<String> toDelKeys = new ArrayList<String>();
            while(keyIter.hasNext()){
                String key = keyIter.next();
                JSONObject jObject = root.getJSONObject(key);
                JSONArray jArray = jObject.getJSONArray("pricesCount");
                int len = jArray.length();
                JSONObject latestDateObj = jArray.getJSONObject(len - 1);
                String latestDate = latestDateObj.getString("date");
                int delCount = 0;
                String curDateStr = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss").format(new Date());


                if(len > 2){
                    for(int i = len - 1; i >=0; i--){
                        String pastDate = jArray.getJSONObject(i).getString("date");
                        long minuteDiff = calMinuteDiff(curDateStr, pastDate);
                        long hourDiff = minuteDiff / 60;
                        if(hourDiff >= 420){
                            delCount = i;
                            break;
                        }
                    }

                    int i = 1;
                    while(i <= delCount){
                        jArray.remove(0);
                        i++;
                    }

                    //todo 如果历史悠久，那么就整个删掉
                    if(jArray.length() == 0){
                        toDelKeys.add(key);
                    }
                }

            }
            for(String toDelKey : toDelKeys){
                root.remove(toDelKey);
            }
            stringToFile(root.toString(), "somejsondata.json");
            stringToFile(root.toString(), "somejsondata " + new SimpleDateFormat("yyyy-MM-dd HH-mm-ss").format(new Date()) +".json");

            Iterator<String> keys = root.keys();
            JSONObject output = new JSONObject();
            JSONObject outputToWatch = new JSONObject();
            //todo 这里还要做个差值对比，昨天，今天，差值，大于某个阈值的显示。判断两次爬取的时间，如果
            //todo 大于二十四小时，那么就。。。相减看看数量少了多少。 超过阈值的请显示
            //todo 判断如果超过48小时数量没有减少的话就删掉。从tocrawllist删掉。
            List<String> toCrawlCardList = getToCrawlList();
//            List<String>  //解决同一个set有一个减了也要维护的问题。
            Map<String, Boolean> toDel = new HashMap<>();//true就是待删除，false就是不能删除

            while(keys.hasNext()){
                boolean isShow = false;
                boolean isCountDiffReached = false;
                boolean isScarceReached = false;
                boolean isNotIgnored = true;
                String key = keys.next();
                 String[] sarr = key.split("\\|");
                String cardName = sarr[0];
                System.out.println("----------- begin------------- = " + cardName);

                if(isContainIgnore(key, kAnalyzeSetIgnores)) isNotIgnored = false;
                JSONObject o = root.getJSONObject(key);

                JSONArray arr =  o.getJSONArray("pricesCount");
                int count = arr.getJSONObject(arr.length() - 1).getInt("count");
                String date = arr.getJSONObject(arr.length() - 1).getString("date");


                if(arr.length() > 2){
                    int begin = arr.length() - 2;
                    boolean isMaxIntervalReached = false;
                    //todo 时间跨度超出四天，就拿对上一次日期作为对比。
                    while(begin >= 0){
                        String lastDate = arr.getJSONObject(begin).getString("date");

                        //这里应该一直往前找，找到尽头，相差24小时以内时间间隔最大的两个的的数量。
                        //todo 有数量差才算时间差

                        long minuteDiff = calMinuteDiff(date, lastDate);
                        long hourDiff = minuteDiff / 60;
                        if(hourDiff > kMaxInterval){
                            isMaxIntervalReached = true;
                            break;
                        }
                        begin--;
                    }

                    if(begin < 0) begin = 0;
                    int lastCount = arr.getJSONObject(begin).getInt("count");
                    String lastDate = arr.getJSONObject(begin).getString("date");

                    long minuteDiff = calMinuteDiff(date, lastDate);
                    long hourDiff = minuteDiff / 60;
                    String toShowDiffStr = minuteDiff + "min";
                    if(minuteDiff > 60){
                        toShowDiffStr = hourDiff + "h";
                        if(hourDiff > 24){
                            int days = (int)hourDiff / 24;
                            int rest = (int)hourDiff % 24;
                            toShowDiffStr = days + "d" + rest + "h";
                        }
                    }
                    int countDiff = count - lastCount;

                    String hourDiffStr = toShowDiffStr;
                    JSONObject dateObj = new JSONObject();
                    dateObj.put("maxInterval", hourDiffStr);
                    dateObj.put("diffCount", countDiff);
                    o.put("pricesCountDiff", dateObj);


                    if(countDiff <= kCountDiffThreshold && kIsConsiderCountDiff ){
                        isCountDiffReached = true;
                    }


                    /**
                     * 删除
                     */
                    Boolean isDel = toDel.get(cardName);
                    if(countDiff >= kRemoveDiffThreshold + 1){
                        System.out.println("数量减太少了");
                    }
                    if(isMaxIntervalReached){
                        System.out.println("也达到了最大interval");
                    }
                    if(isMaxIntervalReached && countDiff >= kRemoveDiffThreshold + 1  && count <= kShowCountThreshold || count > kMaxCountDelThreshold){
                        if(isDel == null){
                            toDel.put(cardName, true);
                            System.out.println("-----------card " + cardName + "   加入了小黑屋");
                        }
                     }else{
                        toDel.put(cardName, false);
                        System.out.println("-----------card " + cardName + "   离开了小黑屋");
                    }
                    System.out.println("----------- end-------------");

                    arr = new JSONArray();
                    JSONObject beginObj = new JSONObject();
                    JSONObject endObj = new JSONObject();
                    beginObj.put("date", date);
                    beginObj.put("count", count);
                    endObj.put("date", lastDate);
                    endObj.put("count", lastCount);
                    arr.put(beginObj);
                    arr.put(endObj);

                    o.put("pricesCount", arr);
                }

                //todo markmark
                //没有remove掉的卡，加上tracked
                //铁牌干掉。爬的时候就。
                //这里还要把数量很少的卡加入另外一个结构并且存起来一个data文件。


                if(count < kShowCountThreshold && kIsConsiderScarce){
                    isScarceReached = true;
                }

                if(kCondition.equals("and") && kIsConsiderCountDiff && kIsConsiderScarce){
                    isShow = isScarceReached && isCountDiffReached && isNotIgnored;
                }else{
                    isShow = (isScarceReached || isCountDiffReached) && isNotIgnored;
                }

                if(isShow){
                    output.put(key, o);
                }


                if(count <= kToWatchThreshold){
                    outputToWatch.put(key, o);
                }
                //todo 这里把价格最新的两个拿出来看看日期，如果不同就拿之前的判断
                //todo 不过有可能半天就差距巨大了，这样吧，就拿最近两次爬取的数量差吧。
                //todo 最新两次爬取的数量差。譬如今天很多牌都变化，那么就半天爬一次我看看对比一下。
                ///todo 但是时间短了阈值就不同了才行
            }

            //todo markmark  amulet of vigor 这张牌12号数量还在减少但是金鱼消失，这种情况要靠保留tocrawlist才行，这里做一下。

            for(String cardNameKey : toDel.keySet()){
                Boolean isRemove = toDel.get(cardNameKey);
                if(isRemove != null){
                    if(isRemove){
                        String currentDir = System.getProperty("user.dir");
                        System.out.println("current dir = " + currentDir);
                        System.out.println("cardNameKey = " + cardNameKey);
                        if(toCrawlCardList == null) System.out.println("null!!!!!!!!!!!!!!!!!!");
                        toCrawlCardList.contains(cardNameKey);
                        toCrawlCardList.remove(cardNameKey);
                        System.out.println("因为所有系列的这个在" + kMaxInterval +"h之内变化少于 " + kRemoveDiffThreshold + " 或者数量大于 " + kMaxCountDelThreshold +" 移除了：" + cardNameKey);
                        String toCrawlListStr = getStringFromToCrawlCardList(toCrawlCardList);
                        stringToFile(toCrawlListStr, "tocrawllist.data");
                    }
                }
            }

            System.out.println("-------------- after to crawl card list size = " + toCrawlCardList.size());

            stringToFile(output.toString(), "analyzeddata.json");
//            outputToWatch
//            stringToFile(toCrawlListStr, "tocrawllist.data" + new SimpleDateFormat("yyyy-MM-dd HH-mm-ss").format(new Date()) +".json");
            stringToFile(outputToWatch.toString(), "towatch.json");
            stringToFile(outputToWatch.toString(), "towatchdata" + new SimpleDateFormat("yyyy-MM-dd HH-mm-ss").format(new Date()) +".json");
            System.out.println("总共筛选出的entry数量：" + output.length());
            System.out.println("---------------分析结束-------------");

        }catch(JSONException e){
            e.printStackTrace();
        }
    }

    private static void crawl(){
//        setSystemConfig();
        DesiredCapabilities capability = DesiredCapabilities.chrome();
//        capability.setBrowserName("chrome");
//        capability.setPlatform(PlatformAndEnvironmentSetUp.platformSetUp);
        System.setProperty("webdriver.chrome.driver", "C:\\develop\\tools\\chromedriver\\chromedriver.exe");//chromedriver服务地址
        long start = System.currentTimeMillis();

        ChromeOptions options = new ChromeOptions();
//        options.addArguments("enable-automation");
//        options.addArguments("--headless");
//        options.addArguments("--window-size=1920,1080");
//        options.addArguments("--no-sandbox");
//        options.addArguments("--disable-extensions");
//        options.addArguments("--dns-prefetch-disable");
//        options.addArguments("--disable-gpu");
//        options.setPageLoadStrategy(PageLoadStrategy.NORMAL);
        WebDriver driver = new ChromeDriver(options);
//        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
//        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
        List<String> toCrawlCardList = getToCrawlList();
        JSONObject root = new JSONObject();

//        这里先把原来的数据读取出来，再把新的数\据加进去才行。


        try {
            String jsonStr = fileToString("somejsondata.json");

            if(jsonStr != null && jsonStr.length() > 0){
                root = new JSONObject(jsonStr);
            }

            for(String toCrawlUrl : goldfishUrls){
                driver.get(toCrawlUrl);//打开指定的网站
                try{
                    Thread.sleep(8000);
                }catch(InterruptedException e){
                    e.printStackTrace();
                }

                driver.get(toCrawlUrl);//打开指定的网站
                try{
                    Thread.sleep(8000);
                }catch(InterruptedException e){
                    e.printStackTrace();
                }

                WebElement tbodyTag = driver.findElement(By.tagName("tbody"));
                List<WebElement> trList = tbodyTag.findElements(By.tagName("tr"));
                System.out.println("-------------list-------------");
                for (WebElement trRoot : trList) {
                    List<WebElement> tdList = trRoot.findElements(By.tagName("td"));
                    WebElement cardNameTD = tdList.get(3);
                    WebElement nameTagA = cardNameTD.findElement(By.tagName("a"));
                    String cardName = nameTagA.getText();
                    System.out.println("card name = " + cardName);

                    if(!cardName.equals("") || cardName != null){
                        if(!toCrawlCardList.contains(cardName)){
                            System.out.println("----------not contained!!!!!!!1---------");
                            toCrawlCardList.add(cardName);
                        }else{
                            System.out.println("----------already contained!~~~~~~~1---------");
                        }
                    }

                }
            }

            String toCrawlListStr = getStringFromToCrawlCardList(toCrawlCardList);

            stringToFile(toCrawlListStr, "tocrawllist.data");
            stringToFile(toCrawlListStr, "tocrawllist.data" + new SimpleDateFormat("yyyy-MM-dd HH-mm-ss").format(new Date()) +".json");

            total = toCrawlCardList.size();
            System.out.println("总共有多少张卡需要访问： " + toCrawlCardList.size());

            for(int j = 0; j < toCrawlCardList.size(); j++){
                String cardName = toCrawlCardList.get(j);
                System.out.println("caccccc  = " + cardName);
                String[] splitParams = cardName.split(" ");
                String finalParam = "";
                for(int i = 0; i < splitParams.length; i++){
                    String s = splitParams[i];
                    finalParam = finalParam + URLEncoder.encode(s, "UTF-8");
                    if(i != splitParams.length - 1){
                        finalParam = finalParam + "%20";
                    }
                }

                String newTcgplayerUrl = String.format(tcgplayerUrl, finalParam);
                System.out.println("tcgplayerUrl = " + newTcgplayerUrl);
                driver.get(newTcgplayerUrl);
//
                try{
                    Thread.sleep(1000);
                }catch(InterruptedException e){
                    e.printStackTrace();
                }

                driver.get(newTcgplayerUrl);

                try{
                    Thread.sleep(1000);
                }catch(InterruptedException e){
                    e.printStackTrace();
                }


//                如果找到id是main-frame-error的东西那么就是要重新get页面
                List<WebElement> errorEleList = driver.findElements(By.id("main-frame-error"));
                if(errorEleList.size() > 0){
                    driver.get(newTcgplayerUrl);

                    try{
                        Thread.sleep(1000);
                    }catch(InterruptedException e){
                        e.printStackTrace();
                    }

                    driver.get(newTcgplayerUrl);
                    try{
                        Thread.sleep(1000);
                    }catch(InterruptedException e){
                        e.printStackTrace();
                    }

                }




                List<WebElement> productListTagList = driver.findElements(By.id("productListResults"));
                String setName = "";
                String prices = "";
                String productPriceStr = "";

//                List<WebElement> detailInfoTagList = driver.findElements(By.className("product-details__info"));
                if(productListTagList.size() > 0){
                    List<WebElement> productList = driver.findElements(By.xpath("//div[@class='product__card']"));
                    System.out.println("productList = " + productList);
                    System.out.println("----------crawling --------   " + cardName);

                    if(productList.size() > 0){
                        for(WebElement e : productList){
                            System.out.println("----------crawling --------   " + cardName);
                            WebElement group = e.findElement(By.xpath(".//a[@class='product__group']"));
                            WebElement productPriceTag = e.findElement(By.xpath(".//dl[@class='product__market-price']"));
                            WebElement productPrice = productPriceTag.findElement(By.tagName("dd"));
                            productPriceStr = productPrice.getText();

                            setName = group.getText();
//                            setName = e.findElement(By.xpath("//div[@class='product__card']")).getText();
                            List<WebElement> aTagList = e.findElements(By.className("product__offers-more-count"));
                            if(aTagList.size() > 0){
                                prices = aTagList.get(0).getText();
                                System.out.println("setname = " + setName);
                                if(isContainIgnore(setName, kSetIgnoreKeywords)) continue;
                                System.out.println("prices = " + prices);
                                System.out.println("------------------sss-----------");
                                String num = prices.split(" ")[0];
                                int intnum = Integer.valueOf(num);

                                updateEntry(root, cardName, setName, intnum, productPriceStr);

                            }

                        }
                    }
                }else{
                    System.out.println("--------------wait------------");
                    try{
                        Thread.sleep(15*1000);
                    }catch (InterruptedException e){
                        e.printStackTrace();
                    }
                    try{
                        new WebDriverWait(driver, 111).until(new ExpectedCondition<Boolean>() {
                            @NullableDecl
                            @Override
                            public Boolean apply(@NullableDecl WebDriver webDriver) {
//                            String t = driver.findElement(By.className("sort-toolbar__total-item-count")).getText();
                                System.out.println("-------------before-------------");
                                List<WebElement> tList = webDriver.findElements(By.className("sort-toolbar__total-item-count"));
                                System.out.println("-------------after-------------");
                                if(tList.size() > 0){
                                    WebElement t = tList.get(0);
                                    if(!t.getText().equals("No Results")){
                                        return true;
                                    }
                                }

                                return false;
                            }
                        });

                    }catch(WebDriverException e){
                        System.out.println("web wait exception!!!!!!!!!!!!");

                        e.printStackTrace();
                        break;
                    }


//                    alert__panel
                    if(driver.findElements(By.className("alert__panel")).size() > 0) {
                        System.out.println("fail to access card = " + cardName);
                        continue;
                    }


                    prices = driver.findElement(By.className("sort-toolbar__total-item-count")).getText();
                    setName = driver.findElements(By.className("product-details__set")).get(0).findElement(By.tagName("a")).getAttribute("innerHTML");

                    if(!setName.contains("(Magic)")){
                        setName = setName + " (Magic)";
                    }
//                    seller-spotlight__pricing
                    if(driver.findElements(By.className("seller-spotlight__price")).size()>0){
                        productPriceStr = driver.findElement(By.className("seller-spotlight__price")).getText();
                    }

                    String num = prices.split(" ")[3];
                    int intnum = Integer.valueOf(num);

                    if(isContainIgnore(setName, kSetIgnoreKeywords)) continue;
                    updateEntry(root, cardName, setName, intnum, productPriceStr);
                }
                inc++;
                System.out.println("已完成百分比：" + ((float)inc / total) * 100 + "%");
            }



            stringToFile(root.toString(), "somejsondata.json");
            stringToFile(root.toString(), "somejsondata " + new SimpleDateFormat("yyyy-MM-dd HH-mm-ss").format(new Date()) +".json");

            System.out.println("----------------- start uploading------------");


            HttpURLConnection urlConn = null;
            URL url = new URL("http://47.112.9.144:8004/upload");
            urlConn = (HttpURLConnection)url.openConnection();
            urlConn.setUseCaches(false);
            urlConn.setDoOutput(true);

            urlConn.setRequestMethod("POST");
            urlConn.setRequestProperty("Content-Type", "application/json");
            OutputStream os = urlConn.getOutputStream();
            byte[] outputBytes = root.toString().getBytes("UTF-8");
            os.write(outputBytes);
            os.close();
            System.out.println("resposne code = " + urlConn.getResponseCode());
            System.out.println("resposne meg = " + urlConn.getResponseMessage());
            System.out.println("------------end uploading000000");
            //todo 最后价格数据分析输出json，数量少于100的，少于50的，时间24小时之内数量减少超过五的十的二十的三十的

            System.out.println("数据爬取完毕~~~~");
            long end = System.currentTimeMillis();

            System.out.println("耗时：" + (end - start)/1000 + "s");

            driver.quit();
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        } finally {
            driver.quit();

        }
    }

    private static void setSystemConfig(){
        System.setProperty("http.proxyHost", "localhost");
        System.setProperty("http.proxyPort", "8888");
        System.setProperty("https.proxyHost", "localhost");
        System.setProperty("https.proxyPort", "8888");
    }

    private static String fileToString(String fileName){
        try{
            File tocrawlfile = new File(kDataRootDir + fileName);
            tocrawlfile.createNewFile();
            FileInputStream fin = new FileInputStream(tocrawlfile);
            BufferedReader reader = new BufferedReader(new InputStreamReader(fin));
            StringBuilder sb = new StringBuilder();
            String line = null;
            while((line = reader.readLine())!= null){
                sb.append(line);
            }
            reader.close();
            fin.close();

            String toCrawlListStr = sb.toString();
            return toCrawlListStr;
        }catch(IOException e){
            e.printStackTrace();
        }
        return null;

    }

    private static void stringToFile(String str, String filePath){
        try{
            File file = new File(kDataRootDir + filePath);
            FileWriter writer = new FileWriter(file);
            writer.write(str);
            writer.flush();
            writer.close();
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    private static void updateEntry(JSONObject root, String cardName, String setName, int intnum, String price){
        JSONObject entryObj = getEntry(root, cardName, setName);
        try{
            if(entryObj == null){
                addEntry(root, cardName, setName, intnum, price);

            }else{
                JSONArray priceArr = entryObj.getJSONArray("pricesCount");
                addPriceCount(priceArr, intnum);
                entryObj.put("price", price);



            }

            stringToFile(root.toString(), "somejsondata.json");

        }catch(JSONException e){
            e.printStackTrace();
        }
    }


    private static void addPriceCount(JSONArray priceArr, int count){
        try{
            JSONObject po = new JSONObject();
            po.put("date", new SimpleDateFormat("yyyy-MM-dd HH-mm-ss").format(new Date()));
            po.put("count", count);
            priceArr.put(po);
        }catch(JSONException e){
            e.printStackTrace();
        }

    }

    private static JSONObject getEntry(JSONObject root, String cardName, String setName){
        try{
            String key = cardName + "|" + setName;
            if(root.has(key)){
                return root.getJSONObject(key);
            }
        }catch(JSONException e){
            e.printStackTrace();
        }

        return null;
    }

    private static void addEntry(JSONObject root, String cardName, String setName, int prices, String price){
        try{
            JSONObject o = new JSONObject();
            o.put("cardName", cardName);
            o.put("setName", setName);
            JSONArray priceArr = new JSONArray();
            addPriceCount(priceArr, prices);
            o.put("pricesCount", priceArr);
            o.put("price", price);

            root.put(cardName + "|" + setName, o);
        }catch(JSONException e){
            e.printStackTrace();
        }
    }

//                        if(isContainIgnore(setName)) continue;
    private static boolean isContainIgnore(String setName, String[] ignores){
        boolean ret = false;
        for(String keyStr : ignores){
            if(setName.contains(keyStr)){
                ret = true;
                break;
            }
        }

        return ret;
    }



    private static long calMinuteDiff(String date, String lastDate){
        try{
            SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
            Date newDate = f.parse(date);
            Date oldDate = f.parse(lastDate);
            long timeDiff = newDate.getTime() - oldDate.getTime();
            long minuteDiff = timeDiff / 1000 / 60 ;
            return minuteDiff;
        }catch(ParseException e){
            e.printStackTrace();
        }

        return 0;

    }


    private static List<String> getToCrawlList(){
        String toCrawlListStr = fileToString("tocrawllist.data");
        String[] splitStr = toCrawlListStr.split(";");

            if(splitStr.length > 1){
            List<String> toCrawlCardListTemp =  Arrays.asList(splitStr);
            return  new ArrayList<>(toCrawlCardListTemp);
        }
            return null;
    }



    private static String getStringFromToCrawlCardList(List<String> toCrawlCardList){
        StringBuilder sbbuild = new StringBuilder();
        for(int i = 0; i < toCrawlCardList.size(); i++){
            sbbuild.append(toCrawlCardList.get(i));
            if(i != toCrawlCardList.size() - 1){
                sbbuild.append(";");
            }
        }

        return sbbuild.toString();
    }


}
