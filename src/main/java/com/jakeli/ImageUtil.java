package com.jakeli;

import java.awt.image.BufferedImage;

public class ImageUtil {

    final static int kHexThreshold = 0x19;
//    final static int kHexThreshold = 0x100;

    /**
     * 图像二值化
     * @param image
     */
    public static void binaryzation(BufferedImage image){
        int NUM = 150;
        for(int x=0 ; x<image.getWidth() ; x++){
            for(int y=0 ; y<image.getHeight() ; y++){
                int pixel = image.getRGB(x, y);
                float red = (pixel & 0xff0000) >> 16;
                float green = (pixel & 0xff00) >> 8;
                float blue = (pixel & 0xff);
                if(red > kHexThreshold || green > kHexThreshold || blue > kHexThreshold){
                    pixel = 0xffffff;
                }
//                pixel = 0xffffff;

//                red = filter(red);
//                green = filter(green);
//                blue = filter(blue);

//                float avg = (red + green + blue)/3;
                image.setRGB(x, y, pixel);
            }
        }
    }

    private static float filter(float color){
        if(color > kHexThreshold){
            color = 0xff;
        }

        return color;
    }

}
