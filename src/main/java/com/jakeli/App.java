package com.jakeli;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.NicelyResynchronizingAjaxController;
import com.gargoylesoftware.htmlunit.ScriptResult;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlElement;
import com.gargoylesoftware.htmlunit.html.HtmlForm;
import com.gargoylesoftware.htmlunit.html.HtmlInput;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.gargoylesoftware.htmlunit.util.Cookie;

import com.gargoylesoftware.htmlunit.util.NameValuePair;
import com.jakeli.ImageUtil;
import com.jakeli.OCR;
import net.sourceforge.tess4j.TesseractException;
import org.w3c.dom.html.HTMLAnchorElement;
import org.w3c.dom.html.HTMLBRElement;
import org.w3c.dom.html.HTMLElement;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * Hello world!
 *
 */
public class App 
{


//    private static String userName ="ZHAORUNKANG";
//    private static String userPasswd = "YX13702588082";

    private static String userName ="ZHAORUNKANG";
    private static String userPasswd = "YX13702588082";
    private static final String loginUrl = "http://app.singlewindow.cn/cas/login?_local_login_flag=1&service=http://app.singlewindow.cn/cas/jump.jsp%3FtoUrl%3DaHR0cDovL2FwcC5zaW5nbGV3aW5kb3cuY24vY2FzL29hdXRoMi4wL2F1dGhvcml6ZT9jbGllbnRfaWQ9MTM2NyZyZXNwb25zZV90eXBlPWNvZGUmcmVkaXJlY3RfdXJpPWh0dHAlM0ElMkYlMkZ3d3cuc2luZ2xld2luZG93LmdkLmNuJTJGcG9ydGFsJTJGc3dPYXV0aExvZ2luLmFjdGlvbiUzRnNlcnZpY2VVcmwlM0Q=&localServerUrl=http://www.singlewindow.gd.cn&localDeliverParaUrl=/portal/getSWParams.action&isLocalLogin=1&localLoginLinkName=5pys5Zyw55m75b2V&localLoginUrl=L3BvcnRhbC9zd09hdXRoTG9naW4uYWN0aW9u&colorA1=FFFFFF&colorA2=60,60,61, 0.5&localRegistryUrl=aHR0cDovL3N3Y2FzLnNpbmdsZXdpbmRvdy5nZC5jbi9nZHN3L2V0cHNSZWdpc3Rlci9ldHBzUmVnaXN0ZXJOb0NhcmRQYWdlLmxlYWQ=";

    public static void main( String[] args )
    {
//        System.out.println(System.getProperty("user.dir"));
        WebClient client = new WebClient(BrowserVersion.CHROME);
        client.getOptions().setThrowExceptionOnScriptError(false);
        client.getOptions().setThrowExceptionOnFailingStatusCode(false);
        client.getOptions().setActiveXNative(false);
        client.getOptions().setCssEnabled(true);
        client.getOptions().setJavaScriptEnabled(true);

        client.setAjaxController(new NicelyResynchronizingAjaxController());
        client.setAlertHandler((page, s )->{
            System.out.println("alert!!!!!!");
        });

        try{
            client.getCookieManager().clearCookies();
            HtmlPage page = client.getPage(loginUrl);

            String cookieSession = client.getCookieManager().getCookie("SESSION").getValue();
//            System.out.println("SELSLSLSLELSLSLS = " + cookieSession);
            String cookieRoute1plat = client.getCookieManager().getCookie("route1plat").getValue();
            String cookieRoutecas = client.getCookieManager().getCookie("routecas").getValue();
            //todo 这里不知道为什么cookies有个key不见了。看看是不是分requrest cookies和response cooies

            Set<Cookie> cookies = client.getCookieManager().getCookies();


            final HtmlForm form = page.getFormByName("fm1");
            form.getInputByName("swy").type(userName);
            form.getInputByName("swm2").type(userPasswd);


            for(int i = 0; i < 1; i++){
                URL url = new URL("http://app.singlewindow.cn/cas/plat_cas_verifycode_gen");
                URLConnection connection = url.openConnection();

                connection.addRequestProperty("Cookies", "SESSION=" + cookieSession);
                connection.addRequestProperty("Cookies", "route1plat=" + cookieRoute1plat);
                connection.addRequestProperty("Cookies", "routecas=" + cookieRoutecas);
                System.out.println(connection.getRequestProperties().toString());

                BufferedImage bufferedImage = ImageIO.read(connection.getInputStream());
                File outputFile = new File("image.jpg");

                ImageUtil.binaryzation(bufferedImage);
                ImageIO.write(bufferedImage, "jpg", outputFile);

                String captcha = null;

                try{
                    captcha = OCR.getTesseract().doOCR(bufferedImage).trim().trim();
                    System.out.println("captcha = " + captcha);
                }catch(TesseractException e){
                    e.printStackTrace();
                }


                if(captcha != null && captcha.matches("[A-Za-z0-9]{4}$")){
//                    //todo 这里把账号密码填上去还有验证码，登录，看看正确错误的信息
                    //todo 并且做相应处理
                    form.getInputByName("verifyCode").type(captcha);
                    HtmlPage loginResultPage = form.getInputByName("submit").click();
                    loginResultPage.initialize();
                    client.waitForBackgroundJavaScript(5000);
                    client.waitForBackgroundJavaScriptStartingBefore(4000);

                    String loginRes = loginResultPage.getWebResponse().getContentAsString();

                    PrintWriter out = new PrintWriter("ooooo.html");

                    String xx = loginResultPage.getElementById("errorMsg").getNodeValue();

                    out.println(page.asXml());
                    out.flush();

//                    System.out.println("Tiele@@@@@@@@@@@@@@@ =  "  + titleStr);

                }
            }

        }catch(IOException e){
            e.printStackTrace();
        }

    }
}
